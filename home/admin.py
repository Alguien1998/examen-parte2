from django.contrib import admin

# Register your models here.
from .models import Funcion, Departamento, Software

admin.site.register(Funcion)
admin.site.register(Departamento)
admin.site.register(Software)