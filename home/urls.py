from django.urls import path

from home import views

urlpatterns = [
    path('list/', views.list, name = "list"),
    path('detail/<int:id>', views.detail, name = "detail"),
    path('create/', views.create, name = "create"),
    path('createD/', views.createD, name = "createD"),
    path('update/<int:id>/', views.update, name="update"),
    path('delete/<int:id>/', views.delete, name="delete"),

    path('listDO/', views.listDO, name = "listDO"),
    path('listS/', views.listS, name = "listS"),
    path('listA/', views.listA, name = "listA"),
    path('listE/', views.listE, name = "listE"),

    path('listFO/', views.listFO, name = "listFO"),
    path('listFV/', views.listFV, name = "listFV"),
    path('listPC/', views.listPC, name = "listPC"),
    path('listPW/', views.listPW, name = "listPW"),
    path('listMI/', views.listMI, name = "listMI"),
    path('listPV/', views.listPV, name = "listPV"),
]