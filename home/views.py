from django.shortcuts import render, redirect

# Create your views here.
from .models import Funcion, Departamento, Software
from .forms import FuncionForm, DepartamentoForm, SoftwareForm


def list(request):
	context = {
	"list_software": Software.objects.all()
	}
	return render(request, "home/list.html", context)

def detail(request, id):
	queryset = Software.objects.get(id = id)
	context = {
	"object": queryset
	}
	return render(request, "home/detail.html", context)

def create(request):
	form = SoftwareForm(request.POST or None)
	if request.user.is_authenticated:
		error = "User Logged"
		if form.is_valid():
			form.save()
			return redirect("list")
	else:
		error = "User Must Be Logged"

	context = {
	"form": form,
	"menssage": error
	}
	return render(request, "home/create.html", context)

def update(request, id):
	software = Software.objects.get(id=id)
	if  request.method == "GET":
		form = SoftwareForm(instance=software )
	else:
		form = SoftwareForm(request.POST, instance=software)
		if form.is_valid():
			form.save()
		return redirect("list")
	context = {
	"form": form
	}
	return render(request, "home/update.html", context)

def delete(request, id):
	software = Software.objects.get(id=id)
	if request.method == "POST":
		software.delete()
		return redirect("list")
	context = {
		"object": software
	}
	return render(request, "home/delete.html", context)

def createD(request):
	form = DepartamentoForm(request.POST or None)
	if request.user.is_authenticated:
		error = "User Logged"
		if form.is_valid():
			form.save()
			return redirect("list")
	else:
		error = "User Must Be Logged"

	context = {
	"form": form,
	"menssage": error
	}
	return render(request, "home/createD.html", context)


def listDO(request):
	context = {
	"list_software": Software.objects.filter(departamento='4')
	}
	return render(request, "home/listDO.html", context)

def listS(request):
	context = {
	"list_software": Software.objects.filter(departamento='3')
	}
	return render(request, "home/listS.html", context)

def listA(request):
	context = {
	"list_software": Software.objects.filter(departamento='2')
	}
	return render(request, "home/listA.html", context)

def listE(request):
	context = {
	"list_software": Software.objects.filter(departamento='1')
	}
	return render(request, "home/listE.html", context)




def listFO(request):
	context = {
	"list_software": Software.objects.filter(funcion='6')
	}
	return render(request, "home/listFO.html", context)

def listFV(request):
	context = {
	"list_software": Software.objects.filter(funcion='5')
	}
	return render(request, "home/listFV.html", context)

def listPC(request):
	context = {
	"list_software": Software.objects.filter(funcion='4')
	}
	return render(request, "home/listPC.html", context)

def listPW(request):
	context = {
	"list_software": Software.objects.filter(funcion='3')
	}
	return render(request, "home/listPW.html", context)

def listMI(request):
	context = {
	"list_software": Software.objects.filter(funcion='2')
	}
	return render(request, "home/listMI.html", context)

def listPV(request):
	context = {
	"list_software": Software.objects.filter(funcion='1')
	}
	return render(request, "home/listPV.html", context)