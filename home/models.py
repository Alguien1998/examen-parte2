# Create your models here.
from django.db import models

# Create your models here.

class Funcion(models.Model):
	nombre_funcion = models.CharField(max_length=50)

	def __str__(self):
		return self.nombre_funcion

class Departamento(models.Model):
	nombre_departamento = models.CharField(max_length=50)

	def __str__(self):
		return self.nombre_departamento

class Software(models.Model):
	nombre = models.CharField(max_length=32)
	funcion = models.ForeignKey(Funcion, on_delete=models.CASCADE)
	departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)
	creador = models.CharField(max_length=50)
	fecha_de_creacion = models.DateField()
	slug = models.SlugField()

	def __str__(self):
		return self.nombre