from django import forms


from .models import Funcion, Departamento, Software

class FuncionForm(forms.ModelForm):
	class Meta:
		model = Funcion
		fields = [
		"nombre_funcion",
		]

class DepartamentoForm(forms.ModelForm):
	class Meta:
		model = Departamento
		fields = [
		"nombre_departamento",
		]

class SoftwareForm(forms.ModelForm):
	class Meta:
		model = Software
		fields = [
		"nombre",
		"funcion",
		"departamento",
		"creador",
		"fecha_de_creacion",
		"slug",
		]